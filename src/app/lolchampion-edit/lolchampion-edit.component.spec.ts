import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LolchampionEditComponent } from './lolchampion-edit.component';

describe('LolchampionEditComponent', () => {
  let component: LolchampionEditComponent;
  let fixture: ComponentFixture<LolchampionEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LolchampionEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LolchampionEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
