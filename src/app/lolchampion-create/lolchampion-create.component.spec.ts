import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LolchampionCreateComponent } from './lolchampion-create.component';

describe('LolchampionCreateComponent', () => {
  let component: LolchampionCreateComponent;
  let fixture: ComponentFixture<LolchampionCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LolchampionCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LolchampionCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
