import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from "../shared/rest-api.service";

@Component({
  selector: 'app-lolchampion-create',
  templateUrl: './lolchampion-create.component.html',
  styleUrls: ['./lolchampion-create.component.css']
})
export class LolchampionCreateComponent implements OnInit {

  @Input() lolchampionDetails = { id: 0, name: '', role: '', difficulty: '' }

  constructor(
    public restApi: RestApiService,
    public router: Router
  ) { }

  ngOnInit() { }

  addLolchampion() {
    this.restApi.createLolchampion(this.lolchampionDetails).subscribe((data: {}) => {
      this.router.navigate(['/lolchampion-list'])
    })
  }

}
