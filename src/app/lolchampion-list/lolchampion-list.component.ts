import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";

@Component({
  selector: 'app-shipper-list',
  templateUrl: './lolchampion-list.component.html',
  styleUrls: ['./lolchampion-list.component.css']
})
export class LolchampionListComponent implements OnInit {

  Lolchampions: any = [];
  constructor(
    public restApi: RestApiService
    ) { }

  ngOnInit(): void {
    this.loadLolchampions()
  }

  loadLolchampions() {
    return this.restApi.getLolchampions().subscribe((data: {}) => {
        this.Lolchampions = data;
    })
  }

  deleteLolchampions(id:any) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.restApi.deleteLolchampion(id).subscribe(data => {
        this.loadLolchampions()
      })
    }
  }

}
