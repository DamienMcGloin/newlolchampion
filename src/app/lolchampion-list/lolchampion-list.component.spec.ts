import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LolchampionListComponent } from './lolchampion-list.component';

describe('LolchampionListComponent', () => {
  let component: LolchampionListComponent;
  let fixture: ComponentFixture<LolchampionListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LolchampionListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LolchampionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
