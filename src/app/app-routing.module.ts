import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LolchampionCreateComponent } from './lolchampion-create/lolchampion-create.component';
import { LolchampionEditComponent } from './lolchampion-edit/lolchampion-edit.component';
import { LolchampionListComponent } from './lolchampion-list/lolchampion-list.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'lolchampion-list' },
  { path: 'lolchampion-create', component: LolchampionCreateComponent },
  { path: 'lolchampion-list', component: LolchampionListComponent },
  { path: 'lolchampion-edit/:id', component: LolchampionEditComponent },
  { path: '**', component: LolchampionListComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
