import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LolchampionCreateComponent } from './lolchampion-create/lolchampion-create.component';
import { LolchampionEditComponent } from './lolchampion-edit/lolchampion-edit.component';
import { LolchampionListComponent } from './lolchampion-list/lolchampion-list.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    LolchampionCreateComponent,
    LolchampionEditComponent,
    LolchampionListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
