import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Lolchampion } from '../shared/lolchampion';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RestApiService {

  //apiURL = 'http://localhost:8080/api/v1/lolchampion/';
  apiURL = 'http://franks-lolchampion-demo-franks-lolchampion-demo.emeadocker65.conygre.com:80/api/v1/lolchampion/';

  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  // HttpClient API get() method
  getLolchampions(): Observable<Lolchampion> {
    return this.http.get<Lolchampion>(this.apiURL)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API get() method
  getLolchampion(id:any): Observable<Lolchampion> {
    return this.http.get<Lolchampion>(this.apiURL + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API post() method
  createLolchampion(lolchampion:Lolchampion): Observable<Lolchampion> {
    return this.http.post<Lolchampion>(this.apiURL + '', JSON.stringify(lolchampion), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API put() method
  updateLolchampion(id:number, lolchampion:Lolchampion): Observable<Lolchampion> {
    return this.http.put<Lolchampion>(this.apiURL + '', JSON.stringify(lolchampion), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API delete() method
  deleteLolchampion(id:number){
    return this.http.delete<Lolchampion>(this.apiURL + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // Error handling
  handleError(error:any) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }
}
